
import './ProductAddForm.css'
import React, { useState } from 'react';

import Button from '../button/Button'


const ProductAddForm = (props) => {

    let { itemList, setBasketItems } = props;
    const [productName, setProductName] = useState('');

    const handleAdd = (itemName) => {

        console.log("item - ", itemName);
        if (!!itemName) {
            console.log("product name", itemName)
            const pIndex = itemList.findIndex(item => item.name === itemName)

            if (pIndex !== -1) {
                const newList = [...itemList]
                newList[pIndex].name = itemList[pIndex].name
                newList[pIndex].quantity = itemList[pIndex].quantity + 1
                setBasketItems(newList)
                setProductName('')
            }
            else {
                setBasketItems([...itemList, { name: itemName, quantity: 1 }])
                setProductName('')
            }
        } else {
            alert("Empty Product")
        }
    }

    const handleChange = (e) => {
        setProductName(e.target.value)
    }

    const handleSubmit = (event) => {
        event.target.reset();
        event.preventDefault();
    }

    const handleClear = () => {
        setBasketItems([])
    }

    return (
        <form onSubmit={handleSubmit}>
            <input type="text" placeholder="add product name" name="product-name" className="product-input" onChange={handleChange} value={productName} />
            <Button btnType={'submit'} text={'Add Item'} color={'primary'} onClick={handleAdd} btnValues={productName} />
            <Button btnType={'button'} text={'Clear Basket'} color={'danger'} onClick={handleClear} />
        </form>

    )
}

export default ProductAddForm;