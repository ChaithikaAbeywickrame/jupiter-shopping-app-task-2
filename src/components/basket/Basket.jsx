import './Basket.css'

import React, { useState } from 'react';

import ProductList from '../productList/ProductList'
import ProductAddForm from '../productAddForm/ProductAddForm'

const Basket = () => {

    const [basketItems, setBasketItems] = useState([]);

    return (
        <div>
            <ProductList setBasketItems={setBasketItems} itemList = {basketItems} />
            <ProductAddForm setBasketItems={setBasketItems} itemList= {basketItems} />
        </div>
    );
}

export default Basket;