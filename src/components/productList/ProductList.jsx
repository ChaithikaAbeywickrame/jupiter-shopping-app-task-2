import './ProductList.css'
import React from 'react';
import Button from '../button/Button'

const ProductList = (props) =>{
    let {itemList, setBasketItems} = props;

    const handleRemove = (index) =>{
        console.log("double",index)
        if (index != null) {
            const newList = [...itemList]
            newList[index].name = itemList[index].name
            newList[index].quantity = itemList[index].quantity -1
            if(newList[index].quantity === 0){
                newList.splice(index,1)
            }
            setBasketItems(newList)
        }
    }
    return(
        <div>
            {itemList && itemList.map((item, index) => {
                if(item.quantity === 0){
                    return(
                        <div key = {index} ></div>
                    )
                }else{
                    return (
                        <div key = {index}>
                            <h2 >{item.name} - {item.quantity}</h2>
                            <Button btnType={'button'} text={'Remove Item'} color={'warning'} onClick={handleRemove} btnValues = {index} />
                        </div>
                    )
                }
            })}
        </div>
    )
}

export default ProductList;