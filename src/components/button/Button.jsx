
import './Button.css'
import React from 'react'

const Button = (props) => {
    let {text, onClick, btnValues, btnType} = props
    let btnStyle = `btn btn-${props.color}`;
    
    return (
        <input type={btnType} className={btnStyle} value={text} onClick={()=> onClick(btnValues)}/>
    );
}

export default Button;
